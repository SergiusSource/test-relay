import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
  GraphQLID,
  GraphQLNonNull,
} from 'graphql';

import {
  connectionDefinitions,
  connectionFromArray,
  connectionArgs,
  mutationWithClientMutationId,
} from 'graphql-relay';

var DB = [
    {
      id: 1,
      user: 'Арам Остимчук',
      message: 'Продам гараж, дорого...',
      date: '2016-01-21'
    },
    {
      id: 2,
      user: 'Алиса Сморчкова',
      message: 'Всем чмоки в этом чатике *__*',
      date: '2016-02-19'
    },
    {
      id: 3,
      user: 'Семён Мамедов',
      message: 'Кто здесь 0_o',
      date: '2016-02-29'
    }
]

var messageType = new GraphQLObjectType({
    name: 'Message',
    fields: () => ({
      id: {type: GraphQLID},
      user: {type: GraphQLString},
      message: {type: GraphQLString},
      date: {type: GraphQLString}
  }),
});

var viewerType = new GraphQLObjectType({
  name: 'Viewer',
  fields: () => ({
    messages: {
      type: messageConnection,
      args: connectionArgs,
      resolve: (_, args) => connectionFromArray(DB, args),
    }
  })
});

var {connectionType: messageConnection, edgeType: MessagesEdge} =
  connectionDefinitions({nodeType: messageType});

var addMessageMutation = mutationWithClientMutationId({
  name: 'AddMessage',
  inputFields: {
    user: { type: new GraphQLNonNull(GraphQLString) },
    message: { type: new GraphQLNonNull(GraphQLString) }
  },
  outputFields: {
    viewer : {
      type: viewerType,
      resolve: () => DB
    }
  },
  mutateAndGetPayload: ({user, message}) => {
    var newMessage = {
      id: DB.length+1,
      message,
      user,
      date: '2016'
    };
    DB.push(newMessage);
    return DB;
  },
});

export const Schema = new GraphQLSchema({
  query: new GraphQLObjectType({
      name: 'Query',
      fields: () => ({
        viewer: {
          type: viewerType,
          resolve: () => DB,
        }
      }),
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
      addMessage: addMessageMutation,
    }),
  }),
});