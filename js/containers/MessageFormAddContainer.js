import React, { PropTypes, Component } from 'react'
import MessageFormAdd from '../components/MessageFormAdd'
import * as userActions from '../actions/UserActions'
import addMessageMutation from '../mutation/AddMessageMutation'
import Relay from 'react-relay'
import { bindActionCreators } from 'redux'
import {compose} from 'redux';
import {connect} from 'react-redux';

export default compose(
  connect(
    (state) => ({
      user: state.user,
    }),
    (dispatch) => ({
      userActions: bindActionCreators(userActions, dispatch)
    }),
    (stateProps, dispatchProps, ownProps) => ({
      onAddBtnClick: (message) => {
        Relay.Store.commitUpdate(
          new addMessageMutation({
            message: message,
            user: stateProps.user.name
          })
        );
        // this.refs.textarea.value = '';
      }
    })
  )
)(MessageFormAdd)


//new Date().toJSON().slice(0,10),