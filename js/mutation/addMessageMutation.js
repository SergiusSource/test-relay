import Relay from 'react-relay';

export default class AddMessageMutation extends Relay.Mutation {

  static fragments = {
    message: () => Relay.QL`
      fragment on Message {
        message
        date
        user
        id
      }
    `,
  };

  getOptimisticResponse() {
    return {

    };
  }
}

